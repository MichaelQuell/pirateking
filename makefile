#Makefile

.PHONY: clean run show
.DEFAULT_GOAL := run

CXX = g++

DEBUG = 1
PERFORMANCE = 
ASSERT = 1
SANITIZE = 1
BUILDTIMING = 1

CXXFLAGS = -std=c++17 -Wall -Wextra -Werror -Wpedantic -Wconversion -Wshadow
LDFLAGS = -lm

#All build files are placed there
OBJDIR := build
#Location of the source files
SRCDIR := src

ifdef DEBUG
CXXFLAGS += -g3 -DDEBUG
endif

ifdef PERFORMANCE
CXXFLAGS += -O3 -flto 
endif

ifndef ASSERT
CXXFLAGS  += -DNDEBUG
endif

ifdef SANITIZE
CXXFLAGS  += -fsanitize=address
endif

ifdef BUILDTIMING
TIMING += /usr/bin/time -f "%e %C"
endif

# Final binary
BIN = main
# Put all auto generated stuff to this build dir.
BUILD_DIR = ./build

# List of all .cpp source files.
CPPSOURCE = $(wildcard src/*.cpp)# $(wildcard dir2/*.cpp)

# All .o files go to build dir.
OBJ = $(CPPSOURCE:%.cpp=$(BUILD_DIR)/%.o)
# Gcc/Clang will create these .d files containing dependencies.
DEP = $(OBJ:%.o=%.d)

DEPMAKE = makefile

# Default target named after the binary.
$(BIN) : $(BUILD_DIR)/$(BIN) $(DEPMAKE)

# Actual target of the binary - depends on all .o files.
$(BUILD_DIR)/$(BIN) : $(OBJ)
	@# Create build directories - same structure as sources.
	@mkdir -p $(@D)
	@# Just link all the object files.
	@$(TIMING) $(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

# Include all .d files
-include $(DEP)

# Build target for every single object file.
# The potential dependency on header files is covered
# by calling `-include $(DEP)`.
$(BUILD_DIR)/%.o : %.cpp $(DEPMAKE)
	@mkdir -p $(@D)
	@# The -MMD flags additionaly creates a .d file with
	@# the same name as the .o file.
	@$(TIMING) $(CXX) $(CXXFLAGS) -MMD -c $< -o $@

run: $(BUILD_DIR)/main $(DEPMAKE)
	@$(TIMING) $(BUILD_DIR)/main 6

#For Makefile debug output
show:
	$(info $$CPPSOURCE is [${CPPSOURCE}])
	$(info $$OBJ is [${OBJ}])
	$(info $$DEP is [${DEP}])
	$(info $$MAKECMDGOALS is [${MAKECMDGOALS}])
	$(info $$BUILD_DIR is [${BUILD_DIR}])
	$(info $$SRCDIR is [${SRCDIR}])

clean: $(DEPENDS)
	rm -rf $(BUILD_DIR)


