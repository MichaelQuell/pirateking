#include "johoho.hh"

#include <iostream>
#include <cassert>
#include <random>
#include <algorithm>
#include <iomanip>
#include <chrono>

JoHoHo::JoHoHo() : deck{Card::Flag1, Card::Flag2, Card::Flag3, Card::Flag4, Card::Flag5,
                        Card::Red1, Card::Red2, Card::Red3, Card::Red4, Card::Red5, Card::Red6, Card::Red7, Card::Red8, Card::Red9, Card::Red10, Card::Red11, Card::Red12, Card::Red13,
                        Card::Yellow1, Card::Yellow2, Card::Yellow3, Card::Yellow4, Card::Yellow5, Card::Yellow6, Card::Yellow7, Card::Yellow8, Card::Yellow9, Card::Yellow10, Card::Yellow11, Card::Yellow12, Card::Yellow13,
                        Card::Blue1, Card::Blue2, Card::Blue3, Card::Blue4, Card::Blue5, Card::Blue6, Card::Blue7, Card::Blue8, Card::Blue9, Card::Blue10, Card::Blue11, Card::Blue12, Card::Blue13,
                        Card::Black1, Card::Black2, Card::Black3, Card::Black4, Card::Black5, Card::Black6, Card::Black7, Card::Black8, Card::Black9, Card::Black10, Card::Black11, Card::Black12, Card::Black13,
                        Card::Mermaid1, Card::Mermaid2,
                        Card::Pirate1, Card::Pirate2, Card::Pirate3, Card::Pirate4, Card::Pirate5,
                        Card::ScaryMary,
                        Card::PirateKing},
                   players{},
                   scoreboard(players),
                   randGenerator(std::chrono::system_clock::now().time_since_epoch().count())
{
    std::shuffle(std::begin(deck), std::end(deck), randGenerator);
}

void JoHoHo::show() const
{
    for (size_t i = 0; i < deck.size(); i++)
    {
        std::cout << deck[i] << " ";
    }
    std::cout << "\n";
}

void JoHoHo::setup(const std::vector<std::string> &n, const size_t &aiPlayers)
{
    players.clear();
    players.resize(n.size() + aiPlayers);
    for (size_t i = 0; i < n.size(); i++)
    {
        players[i].setAi(false);
        players[i].setName(n[i]);
    }
    for (size_t i = 0; i < aiPlayers; i++)
    {
        players[n.size() + i].setAi(true);
        players[n.size() + i].setName("Ai--" + std::to_string(i));
    }
    for (size_t i = 0; i < players.size(); i++)
    {
        players[i].setOthers(players);
    }
}

void JoHoHo::game(const std::vector<std::string> &p, const size_t &aiPlayers)
{
    setup(p, aiPlayers);
    for (size_t i = 0; i < nrounds; i++)
    {
        round(i + 1);
        if (withHumans())
        {
            if (system("clear"))
            {
            }
            showScores();
            std::cout << "Continue -> Press Enter!";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
    showScores();
}

void JoHoHo::round(const size_t &n)
{
    //shuffle the deck and distribute the cards

    std::shuffle(std::begin(deck), std::end(deck), randGenerator);
    for (size_t i = 0; i < players.size(); i++)
    {
        for (size_t j = 0; j < n; j++)
        {
            players[i].addCard(deck[i * n + j]);
        }
    }
    //set the first players
    assert(n != 0);
    firstPlayer = (n - 1) % players.size();
    scoreboard.rounds.push_back(std::vector<Score>(players.size()));

    //get the calls from the players
    std::vector<size_t> calls;
    for (size_t i = 0; i < players.size(); i++)
    {
        scoreboard.rounds.back()[i].called = players[i].getCall((firstPlayer + i) % players.size());
        calls.push_back(scoreboard.rounds.back()[i].called);
    }
    //tell all players all calls
    for (size_t i = 0; i < players.size(); i++)
    {
        players[i].setCalls(calls);
    }
    //play all rounds
    for (size_t j = 0; j < n; j++)
    {
        std::vector<Card> playedCards;
        //get the cards from the players
        for (size_t i = 0; i < players.size(); i++)
        {
            Card c = players[(firstPlayer + i) % players.size()].playCard(playedCards);
            assert(players[(firstPlayer + i) % players.size()].isValidCard(c, playedCards));
            playedCards.push_back(c);
        }
        size_t winner = computeWinner(playedCards);

        //tell the players the result
        for (size_t i = 0; i < players.size(); i++)
        {
            players[i].setPlayed(firstPlayer, (firstPlayer + winner) % players.size(), playedCards);
        }
        firstPlayer = (firstPlayer + winner) % players.size();

        //Bonus points
        if (playedCards[winner].isMermaid())
        {
            for (size_t i = 0; i < players.size(); i++)
            {
                if (playedCards[i].isPirateKing())
                {
                    scoreboard.rounds.back()[firstPlayer].points += 50;
                }
            }
        }

        if (playedCards[winner].isPirateKing())
        {
            for (size_t i = 0; i < players.size(); i++)
            {
                if (playedCards[i].isPirate())
                {
                    scoreboard.rounds.back()[firstPlayer].points += 30;
                }
            }
        }

        scoreboard.rounds.back()[firstPlayer].made++;
    } // all cards played
    const std::vector<long> scores = scoreboard.updateScores();
    for (size_t i = 0; i < players.size(); i++)
    {
        players[i].setScores(scores);
    }
}

size_t JoHoHo::computeWinner(const std::vector<Card> &cards) const
{
    size_t mermaid = cards.size();
    size_t winner = 0;
    for (size_t i = 0; i < cards.size(); i++)
    {
        if (cards[i].isMermaid() && mermaid == cards.size())
        {
            mermaid = i;
        }
        if (!(cards[winner] > cards[i]))
        {
            winner = i;
        }
    }
    if (cards[winner].isPirateKing() && mermaid != cards.size())
    {
        winner = mermaid;
    }

    return winner;
}

void JoHoHo::showScores() const
{
    scoreboard.show();
}

void JoHoHo::showRound(const size_t &winner, const std::vector<Card> &playedCards) const
{
    std::cout << "Played: ";
    for (size_t i = 0; i < players.size(); i++)
    {
        if (i == winner)
        {
            std::cout << std::setw(15) << std::left << "*" + players[(firstPlayer + i) % players.size()].getName() + "*"
                      << " ";
        }
        else
        {
            std::cout << std::setw(15) << std::left << players[(firstPlayer + i) % players.size()].getName() << " ";
        }
    }
    std::cout << "\n        ";
    for (size_t i = 0; i < players.size(); i++)
    {
        if (i == winner)
        {
            std::cout << std::setw(15) << std::left << "*" + playedCards[i].to_string() + "*"
                      << " ";
        }
        else
        {
            std::cout << std::setw(15) << std::left << playedCards[i] << " ";
        }
    }
    std::cout << "\n";
}

bool JoHoHo::withHumans() const
{
    for (size_t i = 0; i < players.size(); i++)
    {
        if (players[i].isAi())
        {
            continue;
        }
        else
        {
            return true;
        }
    }
    return false;
}