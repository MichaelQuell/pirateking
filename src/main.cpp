#include <iostream>
#include <cmath>
#include <cassert>

#include "johoho.hh"

int main(int argc, char *argv[])
{
    if (argc <= 1)
    {
        std::cout << argv[0] << " [HumanPlayer]... <#AiPlayer>\n";
        return EXIT_FAILURE;
    }
    std::vector<std::string> players;

    if (argc >= 2)
    {
        for (int i = 1; i < argc - 1; i++)
        {
            players.push_back(argv[i]); //Human
        }
    }
    //numer of ai players
    size_t aiPlayers = std::strtoul(argv[argc - 1], NULL, 0);

    assert(players.size() + aiPlayers >= 2);
    assert(players.size() + aiPlayers <= 6);
    JoHoHo game;
    //game.show();
    game.game(players, aiPlayers);
    return EXIT_SUCCESS;
}