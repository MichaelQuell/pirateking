#include "scoreboard.hh"

#include <iostream>
#include <cassert>
#include <iomanip>

Scoreboard::Scoreboard(std::vector<Player> &p) : players(p) {}

void Scoreboard::show() const
{
    std::cout << "Players : ";

    for (size_t i = 0; i < players.size(); i++)
    {
        std::cout << std::setw(13) << std::left << players[i].getName() << "|";
    }
    std::cout << "\n";

    for (size_t i = 0; i < rounds.size(); i++)
    {
        std::cout << "Round " << std::right << std::setw(2) << i + 1 << ": ";

        for (size_t j = 0; j < rounds[i].size(); j++)
        {
            std::cout << "   " << std::setw(2) << rounds[i][j].called << " " << std::setw(2) << rounds[i][j].made << " " << std::setw(4) << rounds[i][j].points << " ";
        }
        std::cout << "\n";
    }
    std::cout << "TotalPts:  ";
    for (size_t j = 0; j < players.size(); j++)
    {
        long sum = 0;
        for (size_t i = 0; i < rounds.size(); i++)
        {
            sum += rounds[i][j].points;
        }
        std::cout << "       " << std::setw(5) << sum << "  ";
    }
    std::cout << "\n";
}

void Scoreboard::setup()
{
    rounds.resize(1);
    rounds[0].resize(players.size(), Score{-1, 0, 0});
}

std::vector<long> Scoreboard::updateScores()
{
    std::vector<long> scores;
    for (size_t i = 0; i < players.size(); i++)
    {
        if (rounds.back()[i].called == 0)
        {
            if (rounds.back()[i].made == 0)
            {
                rounds.back()[i].points = rounds.size() * 10;
            }
            else
            {
                rounds.back()[i].points = -rounds.size() * 10;
            }
        }
        else
        {
            if (rounds.back()[i].called == rounds.back()[i].made)
            {
                rounds.back()[i].points += rounds.back()[i].called * 20;
            }
            else
            {
                rounds.back()[i].points = -std::abs(rounds.back()[i].called - rounds.back()[i].made) * 10;
            }
        }
        scores.push_back(rounds.back()[i].points);
    }
    return scores;
}
