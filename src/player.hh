#ifndef PLAYER_HH
#define PLAYER_HH
#include <vector>
#include <string>
#include "card.hh"

class Player
{
public:
private:
    std::vector<Card> cards;
    std::string name;
    bool ai = false;

    //Ai
    size_t called;
    size_t made;
    long score;
    size_t firstPlayer;
    std::vector<Player> others;

public:
    void setName(const std::string &s);
    std::string getName() const;

    void setAi(const bool &b);
    bool isAi() const;
    size_t countHumans() const;
    //inform the player
    void setOthers(const std::vector<Player> &o);
    void setCalls(const std::vector<size_t> &calls);
    void setPlayed(const size_t &first, const size_t &winner, const std::vector<Card> &c);
    void setScores(const std::vector<long> &scores);
    void addCard(const Card &card);
    //input from player
    Card playCard(const std::vector<Card> &playedCards);
    size_t getCall(const size_t &n) const;

    bool isValidCard(const Card &c, const std::vector<Card> &playedCards) const;

private:
    void showCards() const;
    void showOthers() const;
    void clearConsole() const;
};

#endif