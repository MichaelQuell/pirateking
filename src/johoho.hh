#ifndef JOHOHO_HH
#define JOHOHO_HH

#include "player.hh"
#include "card.hh"
#include "scoreboard.hh"

#include <vector>
#include <string>
#include <random>

constexpr size_t nrounds = 10;

class JoHoHo
{
private:
    std::vector<Card> deck;
    std::vector<Player> players;
    Scoreboard scoreboard;

    size_t firstPlayer;
    std::minstd_rand0 randGenerator;
public:
    JoHoHo();
    void game(const std::vector<std::string> &p, const size_t &aiPlayers);
private:
    void show() const;
    void setup(const std::vector<std::string> &n, const size_t &aiPlayers);
    void round(const size_t &n);
    size_t computeWinner(const std::vector<Card> &cards) const;

    void showRound(const size_t &winner, const std::vector<Card> &playedCards) const;
    bool withHumans() const;
    void showScores() const;
};

#endif