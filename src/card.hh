#ifndef CARD_HH
#define CARD_HH
#include <vector>
#include <string>

class Card
{
public:
    enum Value : uint8_t
    {
        Flag1,
        Flag2,
        Flag3,
        Flag4,
        Flag5,
        FlagScaryMary,

        Red1,
        Red2,
        Red3,
        Red4,
        Red5,
        Red6,
        Red7,
        Red8,
        Red9,
        Red10,
        Red11,
        Red12,
        Red13,

        Yellow1,
        Yellow2,
        Yellow3,
        Yellow4,
        Yellow5,
        Yellow6,
        Yellow7,
        Yellow8,
        Yellow9,
        Yellow10,
        Yellow11,
        Yellow12,
        Yellow13,

        Blue1,
        Blue2,
        Blue3,
        Blue4,
        Blue5,
        Blue6,
        Blue7,
        Blue8,
        Blue9,
        Blue10,
        Blue11,
        Blue12,
        Blue13,

        Black1,
        Black2,
        Black3,
        Black4,
        Black5,
        Black6,
        Black7,
        Black8,
        Black9,
        Black10,
        Black11,
        Black12,
        Black13,

        Mermaid1,
        Mermaid2,

        Pirate1,
        Pirate2,
        Pirate3,
        Pirate4,
        Pirate5,
        PirateScaryMary,

        ScaryMary,

        PirateKing
    };

    Card() = default;
    //constexpr Card(const Card &aCard) : value(aCard.value) {}
    constexpr Card(const Card::Value &aCard) : value(aCard) {}

    constexpr operator Value() const { return value; }
    explicit operator bool() = delete; // Prevent usage: if(fruit)
    constexpr bool operator==(const Card &a) const { return value == a.value; }
    constexpr bool operator!=(const Card &a) const { return value != a.value; }

    constexpr bool isFlag() const;
    constexpr bool isRed() const;
    constexpr bool isYellow() const;
    constexpr bool isBlue() const;
    constexpr bool isBlack() const;
    constexpr bool isMermaid() const;
    constexpr bool isPirate() const;
    constexpr bool isScaryMary() const;
    constexpr bool isPirateKing() const;

    constexpr bool isColor() const;

    inline std::string to_string() const;

    constexpr bool operator>(const Card &o) const;

private:
    Value value;
};

inline std::ostream &operator<<(std::ostream &out, const Card &c);

#include "card.hpp"

#endif