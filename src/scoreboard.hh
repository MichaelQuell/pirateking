#ifndef SCOREBOARD_HH
#define SCOREBOARD_HH
#include <vector>
#include <string>
#include <iostream>
#include "player.hh"
#include "card.hh"

struct Score
{
    long called;
    long made;
    long points;
};

class Scoreboard
{

public:
    std::vector<std::vector<Score>> rounds;
    std::vector<Player> &players;

public:
    Scoreboard(std::vector<Player> &p);
    void show() const;
    void setup();
    std::vector<long> updateScores();
};

#endif