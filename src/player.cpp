#include <cassert>
#include <limits>
#include <iomanip>
#include "player.hh"

std::string Player::getName() const { return name; }
void Player::setName(const std::string &s) { name = s; }
void Player::addCard(const Card &card) { cards.push_back(card); }
void Player::showCards() const
{
    std::cout << getName() << " HandCards: ";
    for (size_t i = 0; i < cards.size(); i++)
    {
        std::cout << cards[i] << " ";
    }
    std::cout << "\n";
}

Card Player::playCard(const std::vector<Card> &playedCards)
{
    if (isAi())
    {
        for (size_t i = 0; i < cards.size(); i++)
        {
            Card ret = cards[i];
            if (isValidCard(ret, playedCards))
            {
                cards.erase(cards.begin() + i);
                if (ret.isScaryMary())
                {
                    return Card::PirateScaryMary;
                }
                return ret;
            }
        }
        assert(false);
        return Card::Flag1;
    }
    else
    { //HumanPlays
        clearConsole();
        if (system("clear"))
        {
        }
        showOthers();
        std::cout << "PlayedCards: ";
        for (size_t i = 0; i < playedCards.size(); i++)
        {
            std::cout << playedCards[i] << " ";
        }
        std::cout << "\n";
        showCards();
        std::cout << getName() << " plays: ";
        Card ret;
        size_t i;
        while (true)
        {
            while (true)
            {
                if (std::cin >> i && i < cards.size())
                {
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    break;
                }
                else
                {
                    std::cout << "Out of bounds\n";
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
            }
            ret = cards[i];
            if (isValidCard(ret, playedCards))
            {
                break;
            }
            else
            {
                std::cout << "Against the rules\n";
            }
        }
        cards.erase(cards.begin() + i);
        if (ret.isScaryMary())
        {
            std::cout << "ScaryMary as Pirate (p)?\n";
            char p = '\0';
            while (true)
            {
                if (std::cin >> p)
                {
                    if (p == 'p')
                    {
                        return Card::PirateScaryMary;
                    }
                    else
                    {
                        return Card::FlagScaryMary;
                    }
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    break;
                }
                else
                {
                    std::cout << "Error reading try again\n";
                    std::cin.clear();
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                }
            }
        }
        return ret;
    }
}

void Player::setAi(const bool &b)
{
    ai = b;
}

bool Player::isAi() const
{
    return ai;
}

bool Player::isValidCard(const Card &c, const std::vector<Card> &playedCards) const
{
    if (playedCards.size() == 0)
    {
        return true;
    }
    if (!c.isColor())
    {
        return true;
    }

    for (size_t i = 0; i < playedCards.size(); i++)
    {
        if (playedCards[i].isColor())
        {
            if (playedCards[i].isRed() && !c.isRed())
            {
                for (size_t j = 0; j < cards.size(); j++)
                {
                    if (cards[j].isRed())
                    {
                        return false;
                    }
                }
            }
            if (playedCards[i].isYellow() && !c.isYellow())
            {
                for (size_t j = 0; j < cards.size(); j++)
                {
                    if (cards[j].isYellow())
                    {
                        return false;
                    }
                }
            }
            if (playedCards[i].isBlue() && !c.isBlue())
            {
                for (size_t j = 0; j < cards.size(); j++)
                {
                    if (cards[j].isBlue())
                    {
                        return false;
                    }
                }
            }
            if (playedCards[i].isBlack() && !c.isBlack())
            {
                for (size_t j = 0; j < cards.size(); j++)
                {
                    if (cards[j].isBlack())
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
    return true;
}

size_t Player::getCall(const size_t &n) const
{
    if (isAi())
    {
        size_t call = 0;
        for (size_t i = 0; i < cards.size(); i++)
        {
            if (cards[i].isMermaid() || cards[i].isPirate() || cards[i].isScaryMary() || cards[i].isPirateKing())
            {
                call++;
            }
        }
        if (n == 0 && call < cards.size())
        {
            call++;
        }
        return call;
    }
    else //HumanPlays
    {
        clearConsole();
        if (system("clear"))
        {
        }
        showOthers();
        showCards();
        std::cout << getName() << " calls ";
        size_t i;
        while (true)
        {
            if (std::cin >> i)
            {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                return i;
            }
            else
            {
                std::cout << "Out of bounds\n";
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }
    }
}

void Player::setOthers(const std::vector<Player> &o)
{
    others = o;
    score = 0;
    firstPlayer = 0;
}

void Player::setCalls(const std::vector<size_t> &calls)
{
    for (size_t i = 0; i < others.size(); i++)
    {
        others[i].called = calls[i];
    }
}

void Player::setPlayed(const size_t &first, const size_t &winner, const std::vector<Card> &c)
{
    for (size_t i = 0; i < others.size(); i++)
    {
        others[(first + i) % others.size()].cards.push_back(c[i]);
    }
    firstPlayer = winner;
    others[winner].made++;
}

void Player::showOthers() const
{
    //previously played player names
    std::cout << "PreviouslyPlayed: ";
    for (size_t i = 0; i < others.size(); i++)
    {
        if (i == firstPlayer)
        {
            std::cout << std::setw(15) << std::left << "*" + others[i].getName() + "*"
                      << " ";
        }
        else
        {
            std::cout << std::setw(15) << std::left << others[i].getName() << " ";
        }
    }
    std::cout << "\n                  ";
    //made vs called
    for (size_t i = 0; i < others.size(); i++)
    {
        std::cout << std::setw(15) << std::left << std::to_string(others[i].made) + "/" + std::to_string(others[i].called) << " ";
    }
    std::cout << "\n                  ";
    //previously played cards
    for (size_t j = 0; j < others[0].cards.size(); j++)
    {
        for (size_t i = 0; i < others.size(); i++)
        {
            std::cout << std::setw(15) << std::left << others[i].cards[j] << " ";
        }
        std::cout << "\n                  ";
    }
    std::cout << "\n";
}

void Player::setScores(const std::vector<long> &scores)
{
    for (size_t i = 0; i < others.size(); i++)
    {
        others[i].score = scores[i];
        others[i].called = 0;
        others[i].made = 0;
        others[i].cards.clear();
        ;
    }
}

void Player::clearConsole() const
{
    if (countHumans() > 1)
    {
        if (system("clear"))
        {
        }
        std::cout << getName() << "'s Turn:\nPress Enter!";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
}

size_t Player::countHumans() const
{
    size_t counter = 0;
    for (size_t i = 0; i < others.size(); i++)
    {
        if (!others[i].isAi())
        {
            counter++;
        }
    }
    return counter;
}
