#include "card.hh"

#include <iostream>
#include <cassert>
#include <array>

constexpr bool Card::isFlag() const
{
    switch (value)
    {
    case Flag1:
    case Flag2:
    case Flag3:
    case Flag4:
    case Flag5:
    case FlagScaryMary:
        return true;
        break;
    default:
        return false;
    }
}
constexpr bool Card::isRed() const
{
    switch (value)
    {
    case Red1:
    case Red2:
    case Red3:
    case Red4:
    case Red5:
    case Red6:
    case Red7:
    case Red8:
    case Red9:
    case Red10:
    case Red11:
    case Red12:
    case Red13:
        return true;
        break;
    default:
        return false;
    }
}
constexpr bool Card::isYellow() const
{
    switch (value)
    {
    case Yellow1:
    case Yellow2:
    case Yellow3:
    case Yellow4:
    case Yellow5:
    case Yellow6:
    case Yellow7:
    case Yellow8:
    case Yellow9:
    case Yellow10:
    case Yellow11:
    case Yellow12:
    case Yellow13:
        return true;
        break;
    default:
        return false;
    }
}
constexpr bool Card::isBlue() const
{
    switch (value)
    {
    case Blue1:
    case Blue2:
    case Blue3:
    case Blue4:
    case Blue5:
    case Blue6:
    case Blue7:
    case Blue8:
    case Blue9:
    case Blue10:
    case Blue11:
    case Blue12:
    case Blue13:
        return true;
        break;
    default:
        return false;
    }
}
constexpr bool Card::isBlack() const
{
    switch (value)
    {
    case Black1:
    case Black2:
    case Black3:
    case Black4:
    case Black5:
    case Black6:
    case Black7:
    case Black8:
    case Black9:
    case Black10:
    case Black11:
    case Black12:
    case Black13:
        return true;
        break;
    default:
        return false;
    }
}

constexpr bool Card::isMermaid() const
{
    switch (value)
    {
    case Mermaid1:
    case Mermaid2:
        return true;
        break;
    default:
        return false;
    }
}

constexpr bool Card::isPirate() const
{
    switch (value)
    {
    case Pirate1:
    case Pirate2:
    case Pirate3:
    case Pirate4:
    case Pirate5:
    case PirateScaryMary:
        return true;
        break;
    default:
        return false;
    }
}

constexpr bool Card::isScaryMary() const
{
    switch (value)
    {
    case ScaryMary:
    case FlagScaryMary:
    case PirateScaryMary:
        return true;
        break;
    default:
        return false;
    }
}

constexpr bool Card::isPirateKing() const
{
    switch (value)
    {
    case PirateKing:
        return true;
        break;
    default:
        return false;
    }
}

constexpr bool Card::isColor() const
{
    return isRed() || isYellow() || isBlue() || isBlack();
}

constexpr bool Card::operator>(const Card &o) const
{
    assert(value != ScaryMary);
    assert(o.value != ScaryMary);

    if (isPirateKing())
    {
        return true;
    }
    if (isPirate() && !o.isPirateKing())
    {
        return true;
    }
    if (isMermaid() && !o.isPirateKing() && !o.isPirate())
    {
        return true;
    }
    if (isBlack())
    {
        if (!o.isMermaid() && !o.isPirateKing() && !o.isPirate() && !o.isBlack())
        {
            return true;
        }
        if (o.isBlack())
        {
            return value >= o.value;
        }
    }
    if (isRed())
    {
        if (!o.isMermaid() && !o.isPirateKing() && !o.isPirate() && !o.isBlack() && !o.isRed())
        {
            return true;
        }
        if (o.isRed())
        {
            return value >= o.value;
        }
    }

    if (isYellow())
    {
        if (!o.isMermaid() && !o.isPirateKing() && !o.isPirate() && !o.isBlack() && !o.isYellow())
        {
            return true;
        }
        if (o.isYellow())
        {
            return value >= o.value;
        }
    }
    if (isBlue())
    {
        if (!o.isMermaid() && !o.isPirateKing() && !o.isPirate() && !o.isBlack() && !o.isBlue())
        {
            return true;
        }
        if (o.isBlue())
        {
            return value >= o.value;
        }
    }
    if (isFlag() && o.isFlag())
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::string Card::to_string() const
{
    switch (value)
    {
    case Card::Flag1:
        return "Flag1";
        break;
    case Card::Flag2:
        return "Flag2";
        break;
    case Card::Flag3:
        return "Flag3";
        break;
    case Card::Flag4:
        return "Flag4";
        break;
    case Card::Flag5:
        return "Flag5";
        break;
    case Card::FlagScaryMary:
        return "FlagScaryMary";
        break;

    case Card::Red1:
        return "Red1";
        break;
    case Card::Red2:
        return "Red2";
        break;
    case Card::Red3:
        return "Red3";
        break;
    case Card::Red4:
        return "Red4";
        break;
    case Card::Red5:
        return "Red5";
        break;
    case Card::Red6:
        return "Red6";
        break;
    case Card::Red7:
        return "Red7";
        break;
    case Card::Red8:
        return "Red8";
        break;
    case Card::Red9:
        return "Red9";
        break;
    case Card::Red10:
        return "Red10";
        break;
    case Card::Red11:
        return "Red11";
        break;
    case Card::Red12:
        return "Red12";
        break;
    case Card::Red13:
        return "Red13";
        break;

    case Card::Yellow1:
        return "Yellow1";
        break;
    case Card::Yellow2:
        return "Yellow2";
        break;
    case Card::Yellow3:
        return "Yellow3";
        break;
    case Card::Yellow4:
        return "Yellow4";
        break;
    case Card::Yellow5:
        return "Yellow5";
        break;
    case Card::Yellow6:
        return "Yellow6";
        break;
    case Card::Yellow7:
        return "Yellow7";
        break;
    case Card::Yellow8:
        return "Yellow8";
        break;
    case Card::Yellow9:
        return "Yellow9";
        break;
    case Card::Yellow10:
        return "Yellow10";
        break;
    case Card::Yellow11:
        return "Yellow11";
        break;
    case Card::Yellow12:
        return "Yellow12";
        break;
    case Card::Yellow13:
        return "Yellow13";
        break;

    case Card::Blue1:
        return "Blue1";
        break;
    case Card::Blue2:
        return "Blue2";
        break;
    case Card::Blue3:
        return "Blue3";
        break;
    case Card::Blue4:
        return "Blue4";
        break;
    case Card::Blue5:
        return "Blue5";
        break;
    case Card::Blue6:
        return "Blue6";
        break;
    case Card::Blue7:
        return "Blue7";
        break;
    case Card::Blue8:
        return "Blue8";
        break;
    case Card::Blue9:
        return "Blue9";
        break;
    case Card::Blue10:
        return "Blue10";
        break;
    case Card::Blue11:
        return "Blue11";
        break;
    case Card::Blue12:
        return "Blue12";
        break;
    case Card::Blue13:
        return "Blue13";
        break;

    case Card::Black1:
        return "Black1";
        break;
    case Card::Black2:
        return "Black2";
        break;
    case Card::Black3:
        return "Black3";
        break;
    case Card::Black4:
        return "Black4";
        break;
    case Card::Black5:
        return "Black5";
        break;
    case Card::Black6:
        return "Black6";
        break;
    case Card::Black7:
        return "Black7";
        break;
    case Card::Black8:
        return "Black8";
        break;
    case Card::Black9:
        return "Black9";
        break;
    case Card::Black10:
        return "Black10";
        break;
    case Card::Black11:
        return "Black11";
        break;
    case Card::Black12:
        return "Black12";
        break;
    case Card::Black13:
        return "Black13";
        break;

    case Card::Mermaid1:
        return "Mermaid1";
        break;
    case Card::Mermaid2:
        return "Mermaid2";
        break;

    case Card::Pirate1:
        return "Pirate1";
        break;
    case Card::Pirate2:
        return "Pirate2";
        break;
    case Card::Pirate3:
        return "Pirate3";
        break;
    case Card::Pirate4:
        return "Pirate4";
        break;
    case Card::Pirate5:
        return "Pirate5";
        break;
    case Card::PirateScaryMary:
        return "PirateScaryMary";
        break;

    case Card::ScaryMary:
        return "ScaryMary";
        break;

    case Card::PirateKing:
        return "PirateKing";
        break;
    default:
        assert(false);
        return "<unknown>";
    }
}

inline std::ostream &operator<<(std::ostream &out, const Card &c)
{
    return out << c.to_string();
}
